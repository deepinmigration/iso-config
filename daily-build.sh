#!/bin/bash
PKGVER=${PKGVER:-1.1}

CLONE_URL=$(wget http://10.0.10.32/shuttle/repos/experimental/dists/experimental/main/binary-amd64/Packages.gz 2>/dev/null -O- | zcat | grep 'Filename:\s.*deepin-clone_.*'  | awk '{print $2}')

CLONE_PKG=$(basename ${CLONE_URL})

case $1 in
	"-f")
			FORCE=yes
		;;
	
	"-r")
			REBUILD=yes
		;;
esac

if [ x$REBUILD != xyes ];then
	echo "Save ${CLONE_URL} to cache"
	wget http://10.0.10.32/shuttle/repos/experimental/${CLONE_URL} -O  config/packages.chroot/${CLONE_PKG} 
	if [ -f config/packages.chroot/${CLONE_PKG} ];then
		if [ x${FORCE} = x"yes" ];then
			rm -f config/packages.chroot/deepin-clone_*.deb
		else
			echo "${CLONE_PKG} is alread build"
			exit 0
		fi
	fi
fi

sudo python3 ../livecd.tools/debian.py clean
sudo python3 ../livecd.tools/debian.py build

if [ ! -f binary/live/filesystem.packages ];then
	echo "Build failed"
	sudo python3 ../livecd.tools/debian.py clean
	exit 101
fi

KERVER=$(cat binary/live/filesystem.packages | grep linux-image | head -n 1 | awk '{print $2}' | awk -F'-' '{print $1}')

rm -f live-system.deb
install -Dm644 binary/live/vmlinuz deb.dir/boot/deepin/vmlinuz-${KERVER}
install -Dm644 binary/live/initrd.img deb.dir/boot/deepin/initrd.img-${KERVER}
mkdir -p deb.dir/recovery
sudo mv binary/live/filesystem.* deb.dir/recovery
install -Dm755 config/15_linux_bar deb.dir/etc/grub.d/15_linux_bar
install -Dm755 config/01_live deb.dir/etc/grub.d/01_live
deb_size=$(du deb.dir/ -s | awk '{print $1}')
mkdir deb.dir/DEBIAN
cp -R config/debian/* deb.dir/DEBIAN
sed -i "s/Version: .*/Version: ${PKGVER}+`date +%Y%m%d%H%M`/g" deb.dir/DEBIAN/control
sed -i "s/Installed-Size: .*/Installed-Size: ${deb_size}/g" deb.dir/DEBIAN/control
fakeroot dpkg -b deb.dir live-system.deb
rm -rf deb.dir

sudo python3 ../livecd.tools/debian.py clean
